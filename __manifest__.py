# -*- coding: utf-8 -*-
{
    'name': 'Bill Sales',
    'version': '10.0.0.0.1',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'sale'
    ],
    'data': [
        # views
        'views/sale_order.xml',
    ],
}
