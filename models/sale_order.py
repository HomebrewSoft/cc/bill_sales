# -*- coding: utf-8 -*-
from odoo import _, api, fields, models

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    missing_billing = fields.Float(
        compute='_get_missing_billing',
    )

    @api.depends('amount_total', 'invoice_ids')
    def _get_missing_billing(self):
        for record in self:
            record.missing_billing = record.amount_total - sum(record.invoice_ids.mapped('amount_total_signed'))
            